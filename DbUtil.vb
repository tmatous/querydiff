﻿'  Copyright 2014 Tony Matous
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 


Imports System.Data.Common

Public Class DbUtil

    Public Shared Function GetProviders() As IList(Of String)
        Dim res As New List(Of String)
        Dim provs = DbProviderFactories.GetFactoryClasses()
        For Each row As DataRow In provs.Rows
            res.Add(row.Item("InvariantName").ToString)
        Next
        Return res
    End Function

    Public Shared Function OpenConnection(pConnString As String, pProvider As String) As IDbConnection
        Dim conn = DbProviderFactories.GetFactory(pProvider).CreateConnection()
        conn.ConnectionString = pConnString
        conn.Open()
        Return conn
    End Function


    Public Shared Function SelectDataTable(pConnString As String, pProvider As String, pSql As String) As DataTable
        Dim ds As New DataSet
        Using conn = OpenConnection(pConnString, pProvider)
            Using cmd As IDbCommand = conn.CreateCommand()
                cmd.CommandText = pSql
                Dim da As IDbDataAdapter = DbProviderFactories.GetFactory(pProvider).CreateDataAdapter()
                da.SelectCommand = cmd
                da.Fill(ds)
            End Using
        End Using

        Return ds.Tables.Item(0)
    End Function

    Public Enum eJoinType
        Inner
        Left
    End Enum

    'NOTE: this method offers poor performance for large datasets
    Public Shared Function JoinDataTables(pDt1 As DataTable, pDt2 As DataTable, pCompareFunc As Func(Of DataRow, DataRow, Boolean), Optional pDt1Prefix As String = Nothing, Optional pDt2Prefix As String = Nothing, Optional pLeftJoin As Boolean = False) As DataTable
        If (pDt1Prefix Is Nothing) Then pDt1Prefix = ""
        If (pDt2Prefix Is Nothing) Then pDt2Prefix = ""
        Dim result As New DataTable()
        For Each col As DataColumn In pDt1.Columns
            result.Columns.Add(pDt1Prefix + col.ColumnName, col.DataType)
        Next
        For Each col As DataColumn In pDt2.Columns
            Dim colName = pDt2Prefix + col.ColumnName
            If result.Columns(colName) Is Nothing Then
                result.Columns.Add(colName, col.DataType)
            End If
        Next
        For Each row1lcv As DataRow In pDt1.Rows
            Dim row1 = row1lcv
            Dim joinRows = pDt2.AsEnumerable().Where(Function(row2) pCompareFunc(row1, row2))

            For Each fromRow As DataRow In joinRows
                Dim insertRow As DataRow = result.NewRow()
                For Each col1 As DataColumn In pDt1.Columns
                    insertRow(pDt1Prefix + col1.ColumnName) = row1(col1.ColumnName)
                Next
                For Each col2 As DataColumn In pDt2.Columns
                    insertRow(pDt2Prefix + col2.ColumnName) = fromRow(col2.ColumnName)
                Next
                result.Rows.Add(insertRow)
            Next
            If (pLeftJoin AndAlso (joinRows.Count = 0)) Then
                Dim insertRow As DataRow = result.NewRow()
                For Each col1 As DataColumn In pDt1.Columns
                    insertRow(pDt1Prefix + col1.ColumnName) = row1(col1.ColumnName)
                Next
                result.Rows.Add(insertRow)
            End If
        Next
        Return result
    End Function

    Public Shared Function JoinDataTables(pDt1 As DataTable, pDt2 As DataTable, pJoinColumns As IEnumerable(Of String), pJoinType As eJoinType, Optional pDt1Prefix As String = Nothing, Optional pDt2Prefix As String = Nothing) As DataTable
        If (pDt1Prefix Is Nothing) Then pDt1Prefix = ""
        If (pDt2Prefix Is Nothing) Then pDt2Prefix = ""
        Dim result As New DataTable()
        For Each col As DataColumn In pDt1.Columns
            result.Columns.Add(pDt1Prefix + col.ColumnName, col.DataType)
        Next
        For Each col As DataColumn In pDt2.Columns
            Dim colName = pDt2Prefix + col.ColumnName
            If (Not result.Columns.Contains(colName)) Then
                result.Columns.Add(colName, col.DataType)
            End If
        Next

        Dim sort = ""
        Dim sortDelim = ""
        For Each jc In pJoinColumns
            sort = String.Format("{0}{1} ASC", sortDelim, jc)
            sortDelim = ", "
        Next

        Dim vw1 = New DataView(pDt1)
        vw1.Sort = sort
        Dim vw2 = New DataView(pDt2)
        vw2.Sort = sort
        For Each row1lcv As DataRowView In vw1
            Dim row1 = row1lcv
            Dim filter = ""
            Dim filterDelim = ""
            For Each jc In pJoinColumns
                filter = String.Format("{0}[{1}] = '{2}'", filterDelim, jc, row1.Item(jc).ToString().Replace("'", "''"))
                filterDelim = " AND "
            Next
            vw2.RowFilter = filter

            For Each fromRow As DataRowView In vw2
                Dim insertRow As DataRow = result.NewRow()
                For Each col1 As DataColumn In pDt1.Columns
                    insertRow(pDt1Prefix + col1.ColumnName) = row1(col1.ColumnName)
                Next
                For Each col2 As DataColumn In pDt2.Columns
                    insertRow(pDt2Prefix + col2.ColumnName) = fromRow(col2.ColumnName)
                Next
                result.Rows.Add(insertRow)
            Next
            If (pJoinType = eJoinType.Left AndAlso (vw2.Count = 0)) Then
                Dim insertRow As DataRow = result.NewRow()
                For Each col1 As DataColumn In pDt1.Columns
                    insertRow(pDt1Prefix + col1.ColumnName) = row1(col1.ColumnName)
                Next
                result.Rows.Add(insertRow)
            End If
        Next
        Return result
    End Function

End Class
