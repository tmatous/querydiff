'  Copyright 2014 Tony Matous
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 


Imports System.Configuration
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Text

Public Class Form1

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ddlSource1Type.DataSource = [Enum].GetValues(GetType(Settings.eSourceType))
        ddlSource2Type.DataSource = [Enum].GetValues(GetType(Settings.eSourceType))
        ddlSorted.DataSource = [Enum].GetValues(GetType(Settings.eSorted))
        ddlProvider1.DataSource = DbUtil.GetProviders()
        ddlProvider2.DataSource = DbUtil.GetProviders()
        If (g_StartupLoadFile IsNot Nothing) Then
            Try
                LoadSettings(g_StartupLoadFile)
            Catch ex As Exception
                MsgBox(String.Format("Error loading settings: {0}", ex.Message))
            End Try
        End If
    End Sub


    Private Sub btnCompare_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompare.Click
        DoCompareDr()
    End Sub

    Private Sub DoCompareDr()
        Try
            Dim settings = GetSettingsFromForm()
            Me.Cursor = Cursors.WaitCursor
            Using compare As New QueryCompare()
                Dim myTask = System.Threading.Tasks.Task(Of Boolean).Factory.
                    StartNew(Function()
                                 Return compare.DoCompare(settings)
                             End Function)

                Dim statFrm As New frmStatus()
                statFrm.Show(Me)
                While Not myTask.IsCompleted
                    If (statFrm.RequestCancel) Then compare.RequestCancel()
                    statFrm.SetStatus(compare.Compared, compare.Differences, compare.Elapsed, compare.StatusMessage)
                    Application.DoEvents()
                    System.Threading.Thread.Sleep(500)
                End While

                statFrm.SetStatus(compare.Compared, compare.Differences, compare.Elapsed, compare.StatusMessage)
                statFrm.SetDone()

                If (myTask.Result = False) Then
                    Throw New Exception(compare.Errors)
                Else
                    If (compare.Differences > 0) Then Process.Start(compare.OutputFile)
                End If

            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
            Return
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    'Private Sub DoCompareDt()
    '    Try
    '        Dim keyColumns = (From c In txtKeyFields.Text.Split(","c) Where Not String.IsNullOrWhiteSpace(c) Select c.Trim).ToList
    '        Dim ignoreColumns = (From c In txtIgnoreFields.Text.Split(","c) Where Not String.IsNullOrWhiteSpace(c) Select c.Trim).ToList
    '        If (keyColumns.Count = 0) Then Throw New Exception("A key field is required")
    '        Dim summary As System.Text.StringBuilder
    '        Me.Cursor = Cursors.WaitCursor
    '        Dim dt1 As DataTable
    '        Dim dt2 As DataTable
    '        Using conn1 = DbUtil.OpenConnection(txtDb1.Text, ddlProvider1.Text)
    '            Using conn2 = DbUtil.OpenConnection(txtDb2.Text, ddlProvider2.Text)
    '                dt1 = DbUtil.SelectDataTable(conn1, txtSql1.Text)
    '                dt2 = DbUtil.SelectDataTable(conn2, txtSql2.Text)
    '            End Using
    '        End Using

    '        summary = QueryCompare.CompareDataTables(dt1, dt2,
    '                                                 keyColumns,
    '                                                 ignoreColumns,
    '                                                 pCreateMissingRows:=ckSyncRows.Checked)

    '        Dim frm As New frmCompareDataTables
    '        frm.ShowDataTables(dt1, dt2, ckSyncRows.Checked)

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '        Return
    '    Finally
    '        Me.Cursor = Cursors.Default
    '    End Try
    'End Sub


    Private Sub DoJoin()
        Try
            Dim settings = GetSettingsFromForm()
            Me.Cursor = Cursors.WaitCursor
            Dim dt As DataTable
            Using compare As New QueryCompare()
                dt = compare.DoJoin(settings)
                Dim frm As New frmViewDataTable()
                frm.ShowDataTable(dt)
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
            Return
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub


    Private Sub btnCsv_Click(sender As Object, e As EventArgs) Handles btnCsv1.Click, btnCsv2.Click
        Dim fn = ""
        Using frm As New OpenFileDialog()
            frm.Filter = "CSV Files (*.csv)|*.csv|All Files|*.*"
            If (frm.ShowDialog() <> Windows.Forms.DialogResult.OK) Then Return
            fn = frm.FileName
        End Using
        If (sender.Equals(btnCsv1)) Then
            txtCsv1.Text = fn
        ElseIf (sender.Equals(btnCsv2)) Then
            txtCsv2.Text = fn
        End If
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub LoadSettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LoadSettingsToolStripMenuItem.Click
        Dim fn = ""
        Using frm = New Windows.Forms.OpenFileDialog()
            frm.Filter = "Settings (*.qdcfg)|*.qdcfg|All Files|*.*"
            If (frm.ShowDialog() <> Windows.Forms.DialogResult.OK) Then Return
            fn = frm.FileName
        End Using

        Try
            LoadSettings(fn)
        Catch ex As Exception
            MsgBox(String.Format("Error loading settings: {0}", ex.Message))
        End Try
    End Sub

    Private Sub SaveSettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SaveSettingsToolStripMenuItem.Click
        Dim fn = ""
        Using frm = New Windows.Forms.SaveFileDialog()
            frm.Filter = "Settings (*.qdcfg)|*.qdcfg|All Files|*.*"
            If (frm.ShowDialog() <> Windows.Forms.DialogResult.OK) Then Return
            fn = frm.FileName
        End Using

        Try
            SaveSettings(fn)
        Catch ex As Exception
            MsgBox(String.Format("Error saving settings: {0}", ex.Message))
        End Try
    End Sub

    Private Sub LoadSettings(pFilename As String)
        If (Not IO.File.Exists(pFilename)) Then Throw New Exception("File not found")

        Dim xml = IO.File.ReadAllText(pFilename)
        Dim settings = Util.DeserializeObjectXmlContract(Of Settings)(xml)

        txtKeyFields.Text = String.Join(",", settings.KeyFields)
        txtIgnoreFields.Text = String.Join(",", settings.IgnoreFields)
        txtOutputPath.Text = settings.OutputPath
        ckCoerceTypes.Checked = settings.CoerceTypes
        ckIgnoreCase.Checked = settings.IgnoreCase
        ckTrimStrings.Checked = settings.TrimStrings
        ddlSource1Type.Text = settings.Source1Type.ToString
        ddlSource2Type.Text = settings.Source2Type.ToString
        ddlSorted.Text = settings.Sorted.ToString
        If (settings.Source1 IsNot Nothing) Then
            If (settings.Source1Type = Settings.eSourceType.Database) Then
                Dim src = DirectCast(settings.Source1, Settings.DbSource)
                txtDb1.Text = src.ConnectionString
                ddlProvider1.Text = src.Provider
                txtSql1.Text = src.Query
            ElseIf (settings.Source1Type = Settings.eSourceType.CSV) Then
                Dim src = DirectCast(settings.Source1, Settings.CsvSource)
                txtCsv1.Text = src.Path
            End If
        End If
        If (settings.Source2 IsNot Nothing) Then
            If (settings.Source2Type = Settings.eSourceType.Database) Then
                Dim src = DirectCast(settings.Source2, Settings.DbSource)
                txtDb2.Text = src.ConnectionString
                ddlProvider2.Text = src.Provider
                txtSql2.Text = src.Query
            ElseIf (settings.Source2Type = Settings.eSourceType.CSV) Then
                Dim src = DirectCast(settings.Source2, Settings.CsvSource)
                txtCsv2.Text = src.Path
            End If
        End If
    End Sub


    Private Function GetSettingsFromForm() As Settings
        Dim settings = New Settings
        settings.KeyFields = txtKeyFields.Text.Split(","c).Select(Function(f) f.Trim).ToList
        settings.IgnoreFields = txtIgnoreFields.Text.Split(","c).ToList
        settings.OutputPath = txtOutputPath.Text
        settings.CoerceTypes = ckCoerceTypes.Checked
        settings.IgnoreCase = ckIgnoreCase.Checked
        settings.TrimStrings = ckTrimStrings.Checked
        [Enum].TryParse(ddlSource1Type.Text, settings.Source1Type)
        Select Case settings.Source1Type
            Case Settings.eSourceType.Database
                settings.Source1 = New Settings.DbSource With {
                    .ConnectionString = txtDb1.Text,
                    .Provider = ddlProvider1.Text,
                    .Query = txtSql1.Text
                }
            Case Settings.eSourceType.CSV
                settings.Source1 = New Settings.CsvSource With {
                    .Path = txtCsv1.Text
                }
        End Select
        [Enum].TryParse(ddlSource2Type.Text, settings.Source2Type)
        Select Case settings.Source2Type
            Case Settings.eSourceType.Database
                settings.Source2 = New Settings.DbSource With {
                    .ConnectionString = txtDb2.Text,
                    .Provider = ddlProvider2.Text,
                    .Query = txtSql2.Text
                }
            Case Settings.eSourceType.CSV
                settings.Source2 = New Settings.CsvSource With {
                    .Path = txtCsv2.Text
                }
        End Select
        settings.Sorted = DirectCast([Enum].Parse(GetType(Settings.eSorted), ddlSorted.Text), Settings.eSorted)
        settings.ExportDatasets = ckExportDatasets.Checked

        Return settings
    End Function

    Private Sub SaveSettings(pFilename As String)
        Dim settings = GetSettingsFromForm()
        Dim xml = Util.SerializeObjectXmlContract(settings)
        IO.File.WriteAllText(pFilename, xml)
    End Sub

    Private Sub ddlSource1Type_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSource1Type.SelectedIndexChanged
        UpdateSourceTypes()
    End Sub

    Private Sub ddlSource2Type_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSource2Type.SelectedIndexChanged
        UpdateSourceTypes()
    End Sub

    Private Sub UpdateSourceTypes()
        pnlQuery1.Visible = False
        pnlCsv1.Visible = False
        pnlQuery2.Visible = False
        pnlCsv2.Visible = False
        Dim s1t = Settings.eSourceType.Database
        [Enum].TryParse(ddlSource1Type.Text, s1t)
        Select Case s1t
            Case Settings.eSourceType.Database
                pnlQuery1.Visible = True
            Case Settings.eSourceType.CSV
                pnlCsv1.Visible = True
        End Select
        Dim s2t = Settings.eSourceType.Database
        [Enum].TryParse(ddlSource2Type.Text, s2t)
        Select Case s2t
            Case Settings.eSourceType.Database
                pnlQuery2.Visible = True
            Case Settings.eSourceType.CSV
                pnlCsv2.Visible = True
        End Select
        Me.Refresh()
        pnlSource2.Top = pnlSource1.Top + pnlSource1.Height + 15
        Me.Height = pnlSource2.Bottom + 120
    End Sub

    Private Sub txtSql1_KeyDown(sender As Object, e As KeyEventArgs) Handles txtSql1.KeyDown
        If (e.Control AndAlso e.KeyCode = Keys.A) Then txtSql1.SelectAll() : e.SuppressKeyPress = True
    End Sub

    Private Sub txtSql2_KeyDown(sender As Object, e As KeyEventArgs) Handles txtSql2.KeyDown
        If (e.Control AndAlso e.KeyCode = Keys.A) Then txtSql2.SelectAll() : e.SuppressKeyPress = True
    End Sub

    Private Sub JoinSourcesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles JoinSourcesToolStripMenuItem.Click
        DoJoin()
    End Sub

End Class
