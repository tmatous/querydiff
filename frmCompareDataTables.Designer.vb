'  Copyright 2014 Tony Matous
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 


<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCompareDataTables
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.grdDt1 = New System.Windows.Forms.DataGridView()
        Me.grdDt2 = New System.Windows.Forms.DataGridView()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        CType(Me.grdDt1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdDt2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'grdDt1
        '
        Me.grdDt1.AllowUserToAddRows = False
        Me.grdDt1.AllowUserToDeleteRows = False
        Me.grdDt1.AllowUserToOrderColumns = True
        Me.grdDt1.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.grdDt1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.grdDt1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdDt1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdDt1.Location = New System.Drawing.Point(0, 0)
        Me.grdDt1.Name = "grdDt1"
        Me.grdDt1.ReadOnly = True
        Me.grdDt1.RowHeadersWidth = 60
        Me.grdDt1.Size = New System.Drawing.Size(1059, 350)
        Me.grdDt1.TabIndex = 10
        '
        'grdDt2
        '
        Me.grdDt2.AllowUserToAddRows = False
        Me.grdDt2.AllowUserToDeleteRows = False
        Me.grdDt2.AllowUserToOrderColumns = True
        Me.grdDt2.AllowUserToResizeRows = False
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.grdDt2.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.grdDt2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdDt2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdDt2.Location = New System.Drawing.Point(0, 0)
        Me.grdDt2.Name = "grdDt2"
        Me.grdDt2.ReadOnly = True
        Me.grdDt2.RowHeadersWidth = 60
        Me.grdDt2.Size = New System.Drawing.Size(1059, 347)
        Me.grdDt2.TabIndex = 11
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(2, 3)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.grdDt1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.grdDt2)
        Me.SplitContainer1.Size = New System.Drawing.Size(1059, 701)
        Me.SplitContainer1.SplitterDistance = 350
        Me.SplitContainer1.TabIndex = 12
        '
        'frmCompareDataTables
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1063, 706)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "frmCompareDataTables"
        Me.Text = "Compare Tables"
        CType(Me.grdDt1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdDt2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grdDt1 As System.Windows.Forms.DataGridView
    Friend WithEvents grdDt2 As System.Windows.Forms.DataGridView
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer

End Class
