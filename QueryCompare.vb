﻿'  Copyright 2014 Tony Matous
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 


Public Class QueryCompare
    Implements IDisposable

    Private Const COMPARE_STATUS_COL As String = "_COMPARE_STATUS"

    Private Enum eCompareResult
        Items_EQ = 0
        Item1_GT_Item2 = 1
        Item1_LT_Item2 = -1
    End Enum

    Private Property DataSource1 As IDataSource
    Private Property DataSource2 As IDataSource
    Private Property Settings As Settings
    Public Property OutputFile As String

    Private _compared As Int64
    Public ReadOnly Property Compared() As Int64
        Get
            Return _compared
        End Get
    End Property

    Private _differences As Int64
    Public ReadOnly Property Differences() As Int64
        Get
            Return _differences
        End Get
    End Property

    Private _sw As Stopwatch
    Public ReadOnly Property Elapsed() As TimeSpan
        Get
            If (_sw Is Nothing) Then Return TimeSpan.FromSeconds(0)
            Return _sw.Elapsed
        End Get
    End Property

    Private _statusMessage As String
    Public ReadOnly Property StatusMessage() As String
        Get
            Return _statusMessage
        End Get
    End Property

    Private _errors As String
    Public ReadOnly Property Errors() As String
        Get
            Return _errors
        End Get
    End Property

    Private _RequestCancel As Boolean
    Public Sub RequestCancel()
        SetStatusMessage("Stopping...")
        _RequestCancel = True
    End Sub



    Public Function DoCompare(pSettings As Settings) As Boolean
        Me.Settings = pSettings
        If ((Me.Settings.KeyFields Is Nothing) OrElse (Me.Settings.KeyFields.Count = 0)) Then Throw New Exception("A key field is required")
        If (Me.Settings.IgnoreFields Is Nothing) Then Me.Settings.IgnoreFields = New List(Of String)

        Dim rndStr = (New Random().Next(1000, 9999)).ToString
        Me.OutputFile = IO.Path.Combine(Me.Settings.OutputPath, String.Format("results_{0}.csv", rndStr))

        _RequestCancel = False
        _sw = Stopwatch.StartNew()
        Dim table1CurRow = 0
        Dim table2CurRow = 0

        Try
            SetStatusMessage("Reading sources...")

            LoadSources()

            If (Me.Settings.ExportDatasets) Then
                Me.DataSource1.ExportToFile = IO.Path.Combine(Me.Settings.OutputPath, String.Format("source1_{0}.csv", rndStr))
                Me.DataSource2.ExportToFile = IO.Path.Combine(Me.Settings.OutputPath, String.Format("source2_{0}.csv", rndStr))
            End If
            DataSource1.Open()
            DataSource2.Open()

            For Each curCol In DataSource1.Fields
                If (Not DataSource2.Fields.Contains(curCol, StringComparer.OrdinalIgnoreCase)) Then Throw New Exception("Sources not equivalent")
            Next
            Dim colNames = DataSource1.Fields
            For Each keyCol In pSettings.KeyFields
                If (Not colNames.Contains(keyCol, StringComparer.OrdinalIgnoreCase)) Then Throw New Exception(String.Format("Key field not found: {0}", keyCol))
            Next


            Using outFile = IO.File.CreateText(Me.OutputFile)

                Dim headerVals As New List(Of String)
                headerVals.Add("ROWSTATUS")
                For Each cn In colNames
                    headerVals.Add(String.Format("DS1_{0}", cn))
                    headerVals.Add(String.Format("DS2_{0}", cn))
                Next
                outFile.WriteLine(Util.DelimitedLine(headerVals, ",", """"))


                Dim row1 As IDictionary(Of String, Object) = Nothing
                Dim inc1 = True
                Dim row2 As IDictionary(Of String, Object) = Nothing
                Dim inc2 = True

                Dim rowsCompared As Int64 = 0
                Dim rowsDifferent As Int64 = 0
                SetStatusMessage("Comparing...")
                Do
                    If (_RequestCancel) Then Throw New Exception

                    Dim rowEquality As eCompareResult = eCompareResult.Items_EQ
                    Dim rowStatus = ""
                    If (inc1) Then
                        row1 = DataSource1.ReadNext()
                        table1CurRow += 1
                        inc1 = False
                    End If
                    If (inc2) Then
                        row2 = DataSource2.ReadNext()
                        table2CurRow += 1
                        inc2 = False
                    End If
                    If ((row1 Is Nothing) AndAlso (row2 Is Nothing)) Then Exit Do

                    If (row1 Is Nothing) Then
                        'row is missing from table1
                        rowEquality = eCompareResult.Item1_GT_Item2
                    ElseIf (row2 Is Nothing) Then
                        'row is missing from table2
                        rowEquality = eCompareResult.Item1_LT_Item2
                    Else
                        'compare key columns
                        rowEquality = eCompareResult.Items_EQ
                        For Each ky As String In Me.Settings.KeyFields
                            Dim val1 As Object = row1.Item(ky)
                            Dim val2 As Object = row2.Item(ky)
                            Dim cmp As eCompareResult
                            Try
                                cmp = CompareObjects(val1, val2)
                            Catch ex As Exception
                                Throw New Exception(String.Format("Unable to compare data in column '{0}' : {1}", ky, ex.Message))
                            End Try
                            If (cmp <> eCompareResult.Items_EQ) Then
                                rowEquality = cmp
                                Exit For
                            End If
                        Next
                    End If

                    If (rowEquality = eCompareResult.Item1_LT_Item2) Then
                        'row2 greater, row is missing from table2
                        rowStatus = String.Format("Source 1, row {0} is missing from Source 2", table1CurRow)

                        'increment set 1
                        inc1 = True
                    ElseIf (rowEquality = eCompareResult.Item1_GT_Item2) Then
                        'row1 greater, row is missing from table1
                        rowStatus = String.Format("Source 2, row {0} is missing from Source 1", table2CurRow)

                        'increment set 2
                        inc2 = True
                    Else
                        'key fields same, same row
                        'compare other fields
                        Dim foundUnequal As String = ""
                        Dim foundUnequalDelim As String = ""
                        For Each curFld In colNames
                            If (Me.Settings.KeyFields.Contains(curFld, StringComparer.OrdinalIgnoreCase)) Then Continue For
                            If (Me.Settings.IgnoreFields.Contains(curFld, StringComparer.OrdinalIgnoreCase)) Then Continue For

                            Dim val1 As Object = row1.Item(curFld)
                            Dim val2 As Object = row2.Item(curFld)
                            Dim cmp As eCompareResult
                            Try
                                cmp = CompareObjects(val1, val2)
                            Catch ex As Exception
                                Throw New Exception(String.Format("Unable to compare data in column '{0}' : {1}", curFld, ex.Message))
                            End Try
                            If (cmp <> eCompareResult.Items_EQ) Then
                                'not equal
                                foundUnequal += String.Format("{0}[{1}]", foundUnequalDelim, curFld)
                                foundUnequalDelim = ","
                            End If
                        Next
                        If (foundUnequal <> "") Then
                            rowStatus = String.Format("Source 1, row {0} is different from Source 2, row {1} ({2})", table1CurRow, table2CurRow, foundUnequal)
                        End If

                        'increment both sets
                        inc1 = True
                        inc2 = True
                    End If

                    rowsCompared += 1
                    If (Not String.IsNullOrEmpty(rowStatus)) Then
                        rowsDifferent += 1
                        Dim dataVals As New List(Of String)
                        dataVals.Add(rowStatus)
                        For Each cn In colNames
                            If (rowEquality = eCompareResult.Item1_LT_Item2) Then
                                dataVals.Add(row1.Item(cn).ToString)
                                dataVals.Add("")
                            ElseIf (rowEquality = eCompareResult.Item1_GT_Item2) Then
                                dataVals.Add("")
                                dataVals.Add(row2.Item(cn).ToString)
                            Else
                                dataVals.Add(row1.Item(cn).ToString)
                                dataVals.Add(row2.Item(cn).ToString)
                            End If
                        Next
                        outFile.WriteLine(Util.DelimitedLine(dataVals, ",", """"))
                    End If
                    SetStatus(rowsCompared, rowsDifferent)

                Loop Until (row1 Is Nothing AndAlso row2 Is Nothing)

                outFile.Close()
            End Using
            SetStatusMessage("Done")

            Return True
        Catch ex As Exception
            If (_RequestCancel) Then
                SetStatusMessage("Cancelled")
                _errors = "Compare cancelled"
            ElseIf ((table1CurRow > 0) AndAlso (table2CurRow > 0)) Then
                SetStatusMessage("Error")
                _errors = String.Format("Error comparing Source 1, row {0} and Source 2, row {1} : {2}", table1CurRow, table2CurRow, ex.Message)
            Else
                SetStatusMessage("Error")
                _errors = ex.Message
            End If
            Return False
        Finally
            _sw.Stop()
            If (DataSource1 IsNot Nothing) Then DataSource1.Dispose()
            If (DataSource2 IsNot Nothing) Then DataSource2.Dispose()
            DataSource1 = Nothing
            DataSource2 = Nothing
        End Try
    End Function

    Private Sub LoadSources()
        Select Case Me.Settings.Source1Type
            Case Settings.eSourceType.Database
                Dim cfg = DirectCast(Me.Settings.Source1, Settings.DbSource)
                Me.DataSource1 = New DataReaderSource(cfg.ConnectionString, cfg.Provider, cfg.Query)
            Case Settings.eSourceType.CSV
                Dim cfg = DirectCast(Me.Settings.Source1, Settings.CsvSource)
                Me.DataSource1 = New CsvSource(cfg.Path)
            Case Else : Throw New NotImplementedException
        End Select
        Select Case Me.Settings.Source2Type
            Case Settings.eSourceType.Database
                Dim cfg = DirectCast(Me.Settings.Source2, Settings.DbSource)
                Me.DataSource2 = New DataReaderSource(cfg.ConnectionString, cfg.Provider, cfg.Query)
            Case Settings.eSourceType.CSV
                Dim cfg = DirectCast(Me.Settings.Source2, Settings.CsvSource)
                Me.DataSource2 = New CsvSource(cfg.Path)
            Case Else : Throw New NotImplementedException
        End Select

        If (Me.Settings.Sorted = Settings.eSorted.Unsorted) Then
            'need to load sources into datatables to do sort
            Dim dt1 As New DataTable
            Dim dt2 As New DataTable
            Try
                SetStatusMessage("Loading sources...")
                dt1 = DataSource1.ToDataTable()
                dt2 = DataSource2.ToDataTable()
            Finally
                DataSource1.Dispose()
                DataSource2.Dispose()
                DataSource1 = Nothing
                DataSource2 = Nothing
            End Try

            Dim sort As String = ""
            Dim sortDelim As String = ""
            Dim dt1Clone As DataTable = Nothing
            Dim dt2Clone As DataTable = Nothing
            For Each kc As String In Me.Settings.KeyFields
                If (Me.Settings.IgnoreFields.Contains(kc)) Then Continue For
                sort = String.Format("{0}{1}[{2}] ASC", sort, sortDelim, kc)
                sortDelim = ", "
                If (Me.Settings.CoerceTypes) Then
                    If (dt1.Columns.Contains(kc) AndAlso dt2.Columns.Contains(kc) AndAlso (dt1.Columns.Item(kc).DataType <> dt2.Columns.Item(kc).DataType)) Then
                        'types are different, convert to string
                        If (dt1.Columns.Item(kc).DataType <> GetType(String)) Then
                            If (dt1Clone Is Nothing) Then dt1Clone = dt1.Clone()
                            dt1Clone.Columns(kc).DataType = GetType(String)
                        End If
                        If (dt2.Columns.Item(kc).DataType <> GetType(String)) Then
                            If (dt2Clone Is Nothing) Then dt2Clone = dt2.Clone()
                            dt2Clone.Columns(kc).DataType = GetType(String)
                        End If
                    End If
                End If
            Next
            If (dt1Clone IsNot Nothing) Then
                For Each dr As DataRow In dt1.Rows
                    dt1Clone.ImportRow(dr)
                Next
                dt1 = dt1Clone
            End If
            If (dt2Clone IsNot Nothing) Then
                For Each dr As DataRow In dt2.Rows
                    dt2Clone.ImportRow(dr)
                Next
                dt2 = dt2Clone
            End If

            DataSource1 = New DataTableSource(dt1, sort)
            DataSource2 = New DataTableSource(dt2, sort)
        End If
    End Sub

    Private Sub SetStatus(pCompared As Int64, pDifferent As Int64)
        _compared = pCompared
        _differences = pDifferent
    End Sub

    Private Sub SetStatusMessage(pStatusMessage As String)
        _statusMessage = pStatusMessage
    End Sub


    Private Function CompareObjects(pObj1 As Object, pObj2 As Object) As eCompareResult
        Dim res = CompareObjectsInt(pObj1, pObj2)
        If (Me.Settings.Sorted = Settings.eSorted.Descending) Then
            'reverse result for descending
            If (res = eCompareResult.Item1_LT_Item2) Then
                res = eCompareResult.Item1_GT_Item2
            ElseIf (res = eCompareResult.Item1_GT_Item2) Then
                res = eCompareResult.Item1_LT_Item2
            End If
        End If

        Return res
    End Function

    Private Function CompareObjectsInt(pObj1 As Object, pObj2 As Object) As eCompareResult
        Dim obj1 As Object = pObj1
        Dim obj2 As Object = pObj2
        If (DBNull.Value.Equals(obj1)) Then obj1 = Nothing
        If (DBNull.Value.Equals(obj2)) Then obj2 = Nothing
        If (obj1 Is Nothing OrElse obj2 Is Nothing) Then
            If (obj1 Is Nothing AndAlso obj2 Is Nothing) Then
                Return eCompareResult.Items_EQ
            ElseIf (obj1 Is Nothing) Then
                If (Me.Settings.CoerceTypes AndAlso (TypeOf obj2 Is String) AndAlso (If(Me.Settings.TrimStrings, obj2.ToString.Trim, obj2.ToString) = String.Empty)) Then
                    'evaluate null string and empty string to same thing
                    Return eCompareResult.Items_EQ
                Else
                    Return eCompareResult.Item1_LT_Item2
                End If
            ElseIf (obj2 Is Nothing) Then
                If (Me.Settings.CoerceTypes AndAlso (TypeOf obj1 Is String) AndAlso (If(Me.Settings.TrimStrings, obj1.ToString.Trim, obj1.ToString) = String.Empty)) Then
                    'evaluate null string and empty string to same thing
                    Return eCompareResult.Items_EQ
                Else
                    Return eCompareResult.Item1_GT_Item2
                End If
            Else
                Throw New Exception("Invalid state")
            End If
        Else
            If (obj1.GetType() = obj2.GetType()) Then
                If ((TypeOf obj1 Is String) AndAlso (TypeOf obj1 Is String)) Then
                    'the default GT/LT comparison of strings is ordinal, which is different from the default sort
                    Dim obj1Str = If(Me.Settings.TrimStrings, obj1.ToString.Trim, obj1.ToString)
                    Dim obj2Str = If(Me.Settings.TrimStrings, obj2.ToString.Trim, obj2.ToString)
                    Return DirectCast(String.Compare(obj1Str, obj2Str, ignoreCase:=Me.Settings.IgnoreCase), eCompareResult)
                ElseIf (obj1.Equals(obj2) OrElse (obj1 Is obj2)) Then
                    Return eCompareResult.Items_EQ
                ElseIf (obj1 > obj2) Then
                    Return eCompareResult.Item1_GT_Item2
                Else
                    Return eCompareResult.Item1_LT_Item2
                End If
            ElseIf (Me.Settings.CoerceTypes) Then
                'different type.. convert both to strings and compare
                'TODO: is there a better way?
                Dim obj1Str = If(Me.Settings.TrimStrings, obj1.ToString.Trim, obj1.ToString)
                Dim obj2Str = If(Me.Settings.TrimStrings, obj2.ToString.Trim, obj2.ToString)
                If (obj1Str = obj2Str) Then
                    Return eCompareResult.Items_EQ
                ElseIf (obj1Str > obj2Str) Then
                    Return eCompareResult.Item1_GT_Item2
                Else
                    Return eCompareResult.Item1_LT_Item2
                End If
            Else
                Throw New Exception(String.Format("Different types ({0}, {1})", obj1.GetType().Name, obj2.GetType().Name))
            End If
        End If
    End Function

#Region "Join functionality"

    Public Function DoJoin(pSettings As Settings) As DataTable
        Me.Settings = pSettings
        If ((Me.Settings.KeyFields Is Nothing) OrElse (Me.Settings.KeyFields.Count = 0)) Then Throw New Exception("A key field is required")

        Dim ds1 As IDataSource = Nothing
        Dim ds2 As IDataSource = Nothing
        Dim res As DataTable = Nothing
        Try
            Select Case Me.Settings.Source1Type
                Case Settings.eSourceType.Database
                    Dim cfg = DirectCast(Me.Settings.Source1, Settings.DbSource)
                    ds1 = New DataReaderSource(cfg.ConnectionString, cfg.Provider, cfg.Query)
                Case Settings.eSourceType.CSV
                    Dim cfg = DirectCast(Me.Settings.Source1, Settings.CsvSource)
                    ds1 = New CsvSource(cfg.Path)
                Case Else : Throw New NotImplementedException
            End Select
            Select Case Me.Settings.Source2Type
                Case Settings.eSourceType.Database
                    Dim cfg = DirectCast(Me.Settings.Source2, Settings.DbSource)
                    ds2 = New DataReaderSource(cfg.ConnectionString, cfg.Provider, cfg.Query)
                Case Settings.eSourceType.CSV
                    Dim cfg = DirectCast(Me.Settings.Source2, Settings.CsvSource)
                    ds2 = New CsvSource(cfg.Path)
                Case Else : Throw New NotImplementedException
            End Select

            Dim dt1 = ds1.ToDataTable()
            Dim dt2 = ds2.ToDataTable()
            res = DbUtil.JoinDataTables(dt1, dt2, Me.Settings.KeyFields, DbUtil.eJoinType.Left, "ds1_", "ds2_")

            Return res
        Finally
            If (ds1 IsNot Nothing) Then ds1.Dispose()
            If (ds2 IsNot Nothing) Then ds2.Dispose()
            ds1 = Nothing
            ds2 = Nothing
        End Try
    End Function

#End Region


#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If (DataSource1 IsNot Nothing) Then DataSource1.Dispose()
                If (DataSource2 IsNot Nothing) Then DataSource2.Dispose()
            End If
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
