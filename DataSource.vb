﻿'  Copyright 2014 Tony Matous
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 


Public Interface IDataSource
    Inherits IDisposable
    ReadOnly Property Fields As IList(Of String)
    Property ExportToFile As String
    Sub Open()
    Function ReadNext() As IDictionary(Of String, Object)
    Function ToDataTable() As DataTable
End Interface



Public Class DataReaderSource
    Implements IDataSource

    Private _connString As String
    Private _provider As String
    Private _sql As String

    Private _conn As IDbConnection
    Private _cmd As IDbCommand
    Private _dr As IDataReader
    Private _exportFile As IO.StreamWriter

    Public Sub New(pConnString As String, pProvider As String, pSql As String)
        _connString = pConnString
        _provider = pProvider
        _sql = pSql
    End Sub

    Public Sub Open() Implements IDataSource.Open
        _conn = DbUtil.OpenConnection(_connString, _provider)
        _cmd = _conn.CreateCommand()
        _cmd.CommandText = _sql
        _cmd.CommandTimeout = 60 * 1000 * 10 ' 10 minutes
        _dr = _cmd.ExecuteReader()
        _fields = New List(Of String)
        For curCol = 0 To _dr.FieldCount - 1
            _fields.Add(_dr.GetName(curCol))
        Next
        If (Not String.IsNullOrEmpty(ExportToFile)) Then
            _exportFile = IO.File.CreateText(ExportToFile)
            _exportFile.WriteLine(Util.DelimitedLine(_fields, ",", """"))
        End If
    End Sub

    Private _fields As IList(Of String)
    Public ReadOnly Property Fields() As IList(Of String) Implements IDataSource.Fields
        Get
            Return _fields
        End Get
    End Property

    Private _exportToFile As String
    Public Property ExportToFile As String Implements IDataSource.ExportToFile
        Get
            Return _exportToFile
        End Get
        Set(ByVal value As String)
            _exportToFile = value
        End Set
    End Property



    Public Function ReadNext() As IDictionary(Of String, Object) Implements IDataSource.ReadNext
        If (_dr.Read()) Then
            Dim res As New Dictionary(Of String, Object)(StringComparer.OrdinalIgnoreCase)
            For curCol = 0 To _dr.FieldCount - 1
                res.Add(_dr.GetName(curCol), _dr.GetValue(curCol))
            Next
            If (Not String.IsNullOrEmpty(ExportToFile)) Then
                _exportFile.WriteLine(Util.DelimitedLine(res.Values, ",", """"))
            End If
            Return res
        Else
            Return Nothing
        End If
    End Function


    Public Function ToDataTable() As DataTable Implements IDataSource.ToDataTable
        Return DbUtil.SelectDataTable(_connString, _provider, _sql)
    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If (_dr IsNot Nothing) Then _dr.Dispose()
                If (_cmd IsNot Nothing) Then _cmd.Dispose()
                If (_conn IsNot Nothing) Then _conn.Dispose()
                If (_exportFile IsNot Nothing) Then _exportFile.Dispose()
            End If
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class



Public Class CsvSource
    Implements IDataSource

    Private _sourceFile As String

    Private _rdr As IO.TextReader
    Private _csv As LumenWorks.Framework.IO.Csv.CsvReader
    Private _exportFile As IO.StreamWriter

    Public Sub New(pSourceFile As String)
        _sourceFile = pSourceFile
    End Sub

    Public Sub Open() Implements IDataSource.Open
        _rdr = New IO.StreamReader(_sourceFile)
        _csv = New LumenWorks.Framework.IO.Csv.CsvReader(_rdr, hasHeaders:=True)
        _fields = _csv.GetFieldHeaders.ToList
        If (Not String.IsNullOrEmpty(ExportToFile)) Then
            _exportFile = IO.File.CreateText(ExportToFile)
            _exportFile.WriteLine(Util.DelimitedLine(_fields, ",", """"))
        End If
    End Sub


    Private _fields As IList(Of String)
    Public ReadOnly Property Fields() As IList(Of String) Implements IDataSource.Fields
        Get
            Return _fields
        End Get
    End Property

    Private _exportToFile As String
    Public Property ExportToFile As String Implements IDataSource.ExportToFile
        Get
            Return _exportToFile
        End Get
        Set(ByVal value As String)
            _exportToFile = value
        End Set
    End Property


    Public Function ReadNext() As IDictionary(Of String, Object) Implements IDataSource.ReadNext
        If (_csv.ReadNextRecord()) Then
            Dim res As New Dictionary(Of String, Object)(StringComparer.OrdinalIgnoreCase)
            For curCol = 0 To _csv.FieldCount - 1
                res.Add(_csv.GetFieldHeaders(curCol), _csv.Item(curCol))
            Next
            If (Not String.IsNullOrEmpty(ExportToFile)) Then
                _exportFile.WriteLine(Util.DelimitedLine(res.Values, ",", """"))
            End If
            Return res
        Else
            Return Nothing
        End If
    End Function

    Public Function ToDataTable() As DataTable Implements IDataSource.ToDataTable
        If (_csv Is Nothing) Then Open()
        Dim dt As New DataTable
        For curCol = 0 To _csv.FieldCount - 1
            dt.Columns.Add(_csv.GetFieldHeaders(curCol))
        Next
        Dim vals(_csv.FieldCount - 1) As String
        While (_csv.ReadNextRecord())
            _csv.CopyCurrentRecordTo(vals)
            dt.Rows.Add(vals)
        End While

        Return dt
    End Function


#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If (_csv IsNot Nothing) Then _csv.Dispose()
                If (_rdr IsNot Nothing) Then _rdr.Dispose()
                If (_exportFile IsNot Nothing) Then _exportFile.Dispose()
            End If
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class




Public Class DataTableSource
    Implements IDataSource

    Private _dt As DataTable
    Private _vw As DataView
    Private _curRow As Int32
    Private _sort As String
    Private _exportFile As IO.StreamWriter

    Public Sub New(pDt As DataTable, pSort As String)
        _dt = pDt
        _sort = pSort
    End Sub

    Public Sub Open() Implements IDataSource.Open
        _fields = New List(Of String)
        For curCol = 0 To _dt.Columns.Count - 1
            _fields.Add(_dt.Columns.Item(curCol).ColumnName)
        Next
        If (Not String.IsNullOrEmpty(_sort)) Then
            _vw = New DataView(_dt)
            _vw.Sort = _sort
        Else
            _vw = _dt.DefaultView
        End If
        _curRow = 0
        If (Not String.IsNullOrEmpty(ExportToFile)) Then
            _exportFile = IO.File.CreateText(ExportToFile)
            _exportFile.WriteLine(Util.DelimitedLine(_fields, ",", """"))
        End If
    End Sub


    Private _fields As IList(Of String)
    Public ReadOnly Property Fields() As IList(Of String) Implements IDataSource.Fields
        Get
            Return _fields
        End Get
    End Property

    Private _exportToFile As String
    Public Property ExportToFile As String Implements IDataSource.ExportToFile
        Get
            Return _exportToFile
        End Get
        Set(ByVal value As String)
            _exportToFile = value
        End Set
    End Property



    Public Function ReadNext() As IDictionary(Of String, Object) Implements IDataSource.ReadNext
        If (_curRow >= _vw.Count) Then Return Nothing

        Dim rw = _vw.Item(_curRow)
        _curRow += 1
        Dim res As New Dictionary(Of String, Object)(StringComparer.OrdinalIgnoreCase)
        For curCol = 0 To _fields.Count - 1
            res.Add(_fields.Item(curCol), rw.Item(curCol))
        Next
        If (Not String.IsNullOrEmpty(ExportToFile)) Then
            _exportFile.WriteLine(Util.DelimitedLine(res.Values, ",", """"))
        End If
        Return res
    End Function

    Public Function ToDataTable() As DataTable Implements IDataSource.ToDataTable
        Return _dt
    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If (_exportFile IsNot Nothing) Then _exportFile.Dispose()
            End If
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
