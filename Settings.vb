﻿'  Copyright 2014 Tony Matous
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 


Imports System.Runtime.Serialization

Public Class Settings

    Public Enum eSourceType
        Database
        CSV
    End Enum

    Public Enum eSorted
        Ascending
        Descending
        Unsorted
    End Enum

    <KnownType(GetType(DbSource))>
    <KnownType(GetType(CsvSource))>
    Public MustInherit Class Source
    End Class

    Public Class DbSource
        Inherits Source

        Public Property ConnectionString As String
        Public Property Provider As String
        Public Property Query As String
    End Class

    Public Class CsvSource
        Inherits Source

        Public Property Path As String
    End Class


    Public Property KeyFields As IList(Of String)
    Public Property IgnoreFields As IList(Of String)
    Public Property OutputPath As String
    Public Property CoerceTypes As Boolean
    Public Property IgnoreCase As Boolean
    Public Property TrimStrings As Boolean
    Public Property Sorted As eSorted
    Public Property ExportDatasets As Boolean

    Public Property Source1Type As eSourceType
    Public Property Source1 As Source
    Public Property Source2Type As eSourceType
    Public Property Source2 As Source

End Class
