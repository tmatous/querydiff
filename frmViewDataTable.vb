'  Copyright 2014 Tony Matous
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 


Imports System.Configuration
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Text

Public Class frmViewDataTable
    Dim m_Dt As DataTable = Nothing

    Public Sub ShowDataTable(pDt As DataTable)
        m_Dt = pDt
        grdDt.DataSource = m_Dt
        For Each row As DataGridViewRow In grdDt.Rows
            row.HeaderCell.Value = (row.Index + 1).ToString
        Next
        Me.Show()
    End Sub

End Class
