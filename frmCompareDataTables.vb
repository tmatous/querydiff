'  Copyright 2014 Tony Matous
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 


Imports System.Configuration
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Text

Public Class frmCompareDataTables
    Dim m_Dt1 As DataTable = Nothing
    Dim m_Dt2 As DataTable = Nothing
    Dim m_SyncRows As Boolean = False


    Public Sub ShowDataTables(pDt1 As DataTable, pDt2 As DataTable, pSyncRows As Boolean)
        m_Dt1 = pDt1
        m_Dt2 = pDt2
        m_SyncRows = pSyncRows
        LoadGrids()
    End Sub

    Private Sub LoadGrids()
        grdDt1.DataSource = m_Dt1
        grdDt2.DataSource = m_Dt2
        For Each row As DataGridViewRow In grdDt1.Rows
            row.HeaderCell.Value = (row.Index + 1).ToString
        Next
        For Each row As DataGridViewRow In grdDt2.Rows
            row.HeaderCell.Value = (row.Index + 1).ToString
        Next
        For Each col As DataGridViewColumn In grdDt1.Columns
            col.SortMode = DataGridViewColumnSortMode.NotSortable
        Next
        For Each col As DataGridViewColumn In grdDt2.Columns
            col.SortMode = DataGridViewColumnSortMode.NotSortable
        Next
    End Sub


    Private Sub grid_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles grdDt1.Scroll, grdDt2.Scroll
        If (Not m_SyncRows) Then Return
        Dim target As DataGridView = DirectCast(sender, DataGridView)
        Dim rowIdx As Int32 = target.FirstDisplayedScrollingRowIndex
        If (grdDt1.RowCount > rowIdx) Then grdDt1.FirstDisplayedScrollingRowIndex = target.FirstDisplayedScrollingRowIndex
        If (grdDt2.RowCount > rowIdx) Then grdDt2.FirstDisplayedScrollingRowIndex = target.FirstDisplayedScrollingRowIndex
    End Sub

    Private Sub grid_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdDt1.SelectionChanged, grdDt2.SelectionChanged
        Static _syncingSelection As Boolean = False
        If (Not m_SyncRows) Then Return
        If (_syncingSelection) Then Return
        _syncingSelection = True
        Dim target As DataGridView = DirectCast(sender, DataGridView)
        Dim other As DataGridView = If(target.Equals(grdDt1), grdDt2, grdDt1)
        other.ClearSelection()
        For Each cell As DataGridViewCell In target.SelectedCells
            If ((other.RowCount > cell.RowIndex) AndAlso (other.ColumnCount > cell.ColumnIndex)) Then
                other.Rows(cell.RowIndex).Cells(cell.ColumnIndex).Selected = True
            End If
        Next
        _syncingSelection = False
    End Sub

End Class
