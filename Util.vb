﻿'  Copyright 2014 Tony Matous
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 


Imports System.IO
Imports System.Runtime.Serialization

Public Class Util

    Public Shared Function SerializeObjectXmlContract(pObj As Object) As String
        If (pObj Is Nothing) Then Return ""
        Dim ser As New DataContractSerializer(pObj.GetType())

        Using ms As New MemoryStream()
            ser.WriteObject(ms, pObj)
            Using rdr As New IO.StreamReader(ms, System.Text.Encoding.UTF8)
                ms.Position = 0
                Return rdr.ReadToEnd()
            End Using
        End Using
    End Function


    Public Shared Function DeserializeObjectXmlContract(pXmlStr As String, pType As System.Type) As Object
        If (String.IsNullOrEmpty(pXmlStr)) Then Return Nothing
        Dim ser As New DataContractSerializer(pType)
        Using sr As New StringReader(pXmlStr)
            Using xr = Xml.XmlReader.Create(sr)
                Return ser.ReadObject(xr)
            End Using
        End Using
    End Function

    Public Shared Function DeserializeObjectXmlContract(Of T)(pXmlStr As String) As T
        Return DirectCast(DeserializeObjectXmlContract(pXmlStr, GetType(T)), T)
    End Function

    Public Shared Function DelimitedLine(pFields As IEnumerable, pDelimiter As String, pTextDelimiter As String) As String
        If (String.IsNullOrWhiteSpace(pDelimiter)) Then Throw New Exception("Invalid delimiter")
        If (String.IsNullOrWhiteSpace(pTextDelimiter)) Then Throw New Exception("Invalid text delimiter")

        Dim sb As New System.Text.StringBuilder
        Dim delim = ""
        For Each fld In pFields
            Dim fldStr = Convert.ToString(fld)
            If (fldStr.Contains(pDelimiter) OrElse fldStr.Contains(pTextDelimiter) OrElse fldStr.Contains(vbCr) OrElse fldStr.Contains(vbLf)) Then
                sb.AppendFormat("{0}{1}{2}{1}", delim, pTextDelimiter, fldStr.Replace(pTextDelimiter, pTextDelimiter + pTextDelimiter))
            Else
                sb.AppendFormat("{0}{1}", delim, fldStr)
            End If
            delim = pDelimiter
        Next

        Return sb.ToString
    End Function

End Class
