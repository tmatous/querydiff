﻿'  Copyright 2014 Tony Matous
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 


Public Class frmStatus

    Private _RequestCancel As Boolean
    Public ReadOnly Property RequestCancel As Boolean
        Get
            Return _RequestCancel
        End Get
    End Property


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        lblCompared.Text = ""
        lblDifferences.Text = ""
        btnOk.Visible = False
        btnCancel.Visible = True
        btnCancel.Left = btnOk.Left
    End Sub

    Public Sub SetStatus(pCompared As Int64, pDifferences As Int64, pElapsed As TimeSpan, pStatusMessage As String)
        lblCompared.Text = String.Format("{0:N0}", pCompared)
        lblDifferences.Text = String.Format("{0:N0}", pDifferences)
        lblElapsed.Text = String.Format("{0}:{1:00}", Math.Floor(pElapsed.TotalMinutes), pElapsed.Seconds)
        lblStatus.Text = pStatusMessage
    End Sub

    Public Sub SetDone()
        btnCancel.Visible = False
        btnOk.Visible = True
    End Sub

    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        _RequestCancel = True
        btnCancel.Enabled = False
    End Sub

End Class