﻿Module Module1

    Public g_StartupLoadFile As String = Nothing
    Private _embedDll As New EmbedDll("EmbedDllData")

    Sub Main()
        _embedDll.Register("LumenWorks.Framework.IO")
        Dim args = Environment.GetCommandLineArgs()
        If (args.Length = 2) Then
            g_StartupLoadFile = args(1)
            If (Not IO.File.Exists(g_StartupLoadFile)) Then g_StartupLoadFile = Nothing
        End If
        Dim frm As New Form1
        Application.Run(frm)
    End Sub

End Module
