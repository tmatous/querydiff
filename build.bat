@echo off
set config=Debug
set destdir=.\Build\
echo Ready to copy the %config% assemblies to the %destdir% directory...

rem 64-bit
set MSBuildBinPath=C:\Windows\Microsoft.NET\Framework64\v4.0.30319\
rem 32-bit
rem set MSBuildBinPath=C:\Windows\Microsoft.NET\Framework\v4.0.30319\

%MSBuildBinPath%msbuild.exe /t:Build /p:Configuration=%config% QueryDiff.sln
if ERRORLEVEL 1 goto Error

md %destdir%
copy bin\%config%\QueryDiff.exe %destdir%
rem copy bin\%config%\QueryDiff.exe.config %destdir%
rem copy Components\*.* %destdir%

echo Done.
pause

goto End
:Error
echo Error!
pause
:End
