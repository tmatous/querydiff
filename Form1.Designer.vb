'  Copyright 2014 Tony Matous
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 


<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCompare = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDb1 = New System.Windows.Forms.TextBox()
        Me.txtDb2 = New System.Windows.Forms.TextBox()
        Me.txtSql1 = New System.Windows.Forms.TextBox()
        Me.txtSql2 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtKeyFields = New System.Windows.Forms.TextBox()
        Me.txtIgnoreFields = New System.Windows.Forms.TextBox()
        Me.ddlProvider1 = New System.Windows.Forms.ComboBox()
        Me.ddlProvider2 = New System.Windows.Forms.ComboBox()
        Me.pnlQuery1 = New System.Windows.Forms.Panel()
        Me.pnlQuery2 = New System.Windows.Forms.Panel()
        Me.pnlCsv1 = New System.Windows.Forms.Panel()
        Me.txtCsv1 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnCsv1 = New System.Windows.Forms.Button()
        Me.btnCsv2 = New System.Windows.Forms.Button()
        Me.txtCsv2 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.pnlCsv2 = New System.Windows.Forms.Panel()
        Me.ckCoerceTypes = New System.Windows.Forms.CheckBox()
        Me.ckIgnoreCase = New System.Windows.Forms.CheckBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoadSettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveSettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddlSource1Type = New System.Windows.Forms.ComboBox()
        Me.pnlSource1 = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.pnlSource2 = New System.Windows.Forms.GroupBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ddlSource2Type = New System.Windows.Forms.ComboBox()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ckTrimStrings = New System.Windows.Forms.CheckBox()
        Me.txtOutputPath = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ckExportDatasets = New System.Windows.Forms.CheckBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.ddlSorted = New System.Windows.Forms.ComboBox()
        Me.ToolsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JoinSourcesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.pnlQuery1.SuspendLayout()
        Me.pnlQuery2.SuspendLayout()
        Me.pnlCsv1.SuspendLayout()
        Me.pnlCsv2.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.pnlSource1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.pnlSource2.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCompare
        '
        Me.btnCompare.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCompare.Location = New System.Drawing.Point(787, 774)
        Me.btnCompare.Name = "btnCompare"
        Me.btnCompare.Size = New System.Drawing.Size(75, 23)
        Me.btnCompare.TabIndex = 7
        Me.btnCompare.Text = "Compare"
        Me.btnCompare.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(25, 13)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "DB:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 6)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(25, 13)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "DB:"
        '
        'txtDb1
        '
        Me.txtDb1.Location = New System.Drawing.Point(50, 1)
        Me.txtDb1.Name = "txtDb1"
        Me.txtDb1.Size = New System.Drawing.Size(603, 20)
        Me.txtDb1.TabIndex = 1
        '
        'txtDb2
        '
        Me.txtDb2.Location = New System.Drawing.Point(50, 3)
        Me.txtDb2.Name = "txtDb2"
        Me.txtDb2.Size = New System.Drawing.Size(603, 20)
        Me.txtDb2.TabIndex = 3
        '
        'txtSql1
        '
        Me.txtSql1.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSql1.Location = New System.Drawing.Point(2, 42)
        Me.txtSql1.MaxLength = 0
        Me.txtSql1.Multiline = True
        Me.txtSql1.Name = "txtSql1"
        Me.txtSql1.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtSql1.Size = New System.Drawing.Size(838, 197)
        Me.txtSql1.TabIndex = 2
        '
        'txtSql2
        '
        Me.txtSql2.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSql2.Location = New System.Drawing.Point(4, 45)
        Me.txtSql2.MaxLength = 0
        Me.txtSql2.Multiline = True
        Me.txtSql2.Name = "txtSql2"
        Me.txtSql2.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtSql2.Size = New System.Drawing.Size(838, 197)
        Me.txtSql2.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(5, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 13)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "SQL"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(5, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(28, 13)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "SQL"
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(18, 731)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 13)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Key fields:"
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 755)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 13)
        Me.Label6.TabIndex = 23
        Me.Label6.Text = "Ignore fields:"
        '
        'txtKeyFields
        '
        Me.txtKeyFields.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtKeyFields.Location = New System.Drawing.Point(79, 728)
        Me.txtKeyFields.Name = "txtKeyFields"
        Me.txtKeyFields.Size = New System.Drawing.Size(450, 20)
        Me.txtKeyFields.TabIndex = 24
        '
        'txtIgnoreFields
        '
        Me.txtIgnoreFields.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtIgnoreFields.Location = New System.Drawing.Point(79, 752)
        Me.txtIgnoreFields.Name = "txtIgnoreFields"
        Me.txtIgnoreFields.Size = New System.Drawing.Size(450, 20)
        Me.txtIgnoreFields.TabIndex = 25
        '
        'ddlProvider1
        '
        Me.ddlProvider1.FormattingEnabled = True
        Me.ddlProvider1.Location = New System.Drawing.Point(659, 0)
        Me.ddlProvider1.Name = "ddlProvider1"
        Me.ddlProvider1.Size = New System.Drawing.Size(183, 21)
        Me.ddlProvider1.TabIndex = 26
        '
        'ddlProvider2
        '
        Me.ddlProvider2.FormattingEnabled = True
        Me.ddlProvider2.Location = New System.Drawing.Point(659, 3)
        Me.ddlProvider2.Name = "ddlProvider2"
        Me.ddlProvider2.Size = New System.Drawing.Size(183, 21)
        Me.ddlProvider2.TabIndex = 27
        '
        'pnlQuery1
        '
        Me.pnlQuery1.Controls.Add(Me.Label1)
        Me.pnlQuery1.Controls.Add(Me.txtDb1)
        Me.pnlQuery1.Controls.Add(Me.txtSql1)
        Me.pnlQuery1.Controls.Add(Me.Label3)
        Me.pnlQuery1.Controls.Add(Me.ddlProvider1)
        Me.pnlQuery1.Location = New System.Drawing.Point(3, 38)
        Me.pnlQuery1.Name = "pnlQuery1"
        Me.pnlQuery1.Size = New System.Drawing.Size(845, 248)
        Me.pnlQuery1.TabIndex = 30
        '
        'pnlQuery2
        '
        Me.pnlQuery2.Controls.Add(Me.Label2)
        Me.pnlQuery2.Controls.Add(Me.txtDb2)
        Me.pnlQuery2.Controls.Add(Me.txtSql2)
        Me.pnlQuery2.Controls.Add(Me.Label4)
        Me.pnlQuery2.Controls.Add(Me.ddlProvider2)
        Me.pnlQuery2.Location = New System.Drawing.Point(3, 38)
        Me.pnlQuery2.Name = "pnlQuery2"
        Me.pnlQuery2.Size = New System.Drawing.Size(845, 248)
        Me.pnlQuery2.TabIndex = 31
        '
        'pnlCsv1
        '
        Me.pnlCsv1.Controls.Add(Me.txtCsv1)
        Me.pnlCsv1.Controls.Add(Me.Label8)
        Me.pnlCsv1.Controls.Add(Me.btnCsv1)
        Me.pnlCsv1.Location = New System.Drawing.Point(3, 3)
        Me.pnlCsv1.Name = "pnlCsv1"
        Me.pnlCsv1.Size = New System.Drawing.Size(600, 29)
        Me.pnlCsv1.TabIndex = 32
        '
        'txtCsv1
        '
        Me.txtCsv1.Location = New System.Drawing.Point(50, 2)
        Me.txtCsv1.Name = "txtCsv1"
        Me.txtCsv1.Size = New System.Drawing.Size(503, 20)
        Me.txtCsv1.TabIndex = 34
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(7, 6)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(26, 13)
        Me.Label8.TabIndex = 33
        Me.Label8.Text = "File:"
        '
        'btnCsv1
        '
        Me.btnCsv1.Location = New System.Drawing.Point(559, 3)
        Me.btnCsv1.Name = "btnCsv1"
        Me.btnCsv1.Size = New System.Drawing.Size(27, 20)
        Me.btnCsv1.TabIndex = 35
        Me.btnCsv1.Text = "..."
        Me.btnCsv1.UseVisualStyleBackColor = True
        '
        'btnCsv2
        '
        Me.btnCsv2.Location = New System.Drawing.Point(559, 3)
        Me.btnCsv2.Name = "btnCsv2"
        Me.btnCsv2.Size = New System.Drawing.Size(27, 20)
        Me.btnCsv2.TabIndex = 38
        Me.btnCsv2.Text = "..."
        Me.btnCsv2.UseVisualStyleBackColor = True
        '
        'txtCsv2
        '
        Me.txtCsv2.Location = New System.Drawing.Point(50, 3)
        Me.txtCsv2.Name = "txtCsv2"
        Me.txtCsv2.Size = New System.Drawing.Size(503, 20)
        Me.txtCsv2.TabIndex = 37
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 6)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(26, 13)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "File:"
        '
        'pnlCsv2
        '
        Me.pnlCsv2.Controls.Add(Me.txtCsv2)
        Me.pnlCsv2.Controls.Add(Me.btnCsv2)
        Me.pnlCsv2.Controls.Add(Me.Label9)
        Me.pnlCsv2.Location = New System.Drawing.Point(3, 3)
        Me.pnlCsv2.Name = "pnlCsv2"
        Me.pnlCsv2.Size = New System.Drawing.Size(600, 29)
        Me.pnlCsv2.TabIndex = 39
        '
        'ckCoerceTypes
        '
        Me.ckCoerceTypes.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ckCoerceTypes.AutoSize = True
        Me.ckCoerceTypes.Location = New System.Drawing.Point(562, 728)
        Me.ckCoerceTypes.Name = "ckCoerceTypes"
        Me.ckCoerceTypes.Size = New System.Drawing.Size(92, 17)
        Me.ckCoerceTypes.TabIndex = 41
        Me.ckCoerceTypes.Text = "Coerce Types"
        Me.ckCoerceTypes.UseVisualStyleBackColor = True
        '
        'ckIgnoreCase
        '
        Me.ckIgnoreCase.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ckIgnoreCase.AutoSize = True
        Me.ckIgnoreCase.Location = New System.Drawing.Point(562, 743)
        Me.ckIgnoreCase.Name = "ckIgnoreCase"
        Me.ckIgnoreCase.Size = New System.Drawing.Size(83, 17)
        Me.ckIgnoreCase.TabIndex = 42
        Me.ckIgnoreCase.Text = "Ignore Case"
        Me.ckIgnoreCase.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ToolsToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(874, 24)
        Me.MenuStrip1.TabIndex = 43
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LoadSettingsToolStripMenuItem, Me.SaveSettingsToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(35, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'LoadSettingsToolStripMenuItem
        '
        Me.LoadSettingsToolStripMenuItem.Name = "LoadSettingsToolStripMenuItem"
        Me.LoadSettingsToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.LoadSettingsToolStripMenuItem.Text = "Load Settings"
        '
        'SaveSettingsToolStripMenuItem
        '
        Me.SaveSettingsToolStripMenuItem.Name = "SaveSettingsToolStripMenuItem"
        Me.SaveSettingsToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.SaveSettingsToolStripMenuItem.Text = "Save Settings"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'ddlSource1Type
        '
        Me.ddlSource1Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlSource1Type.FormattingEnabled = True
        Me.ddlSource1Type.Location = New System.Drawing.Point(62, 19)
        Me.ddlSource1Type.Name = "ddlSource1Type"
        Me.ddlSource1Type.Size = New System.Drawing.Size(186, 21)
        Me.ddlSource1Type.TabIndex = 44
        '
        'pnlSource1
        '
        Me.pnlSource1.AutoSize = True
        Me.pnlSource1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.pnlSource1.Controls.Add(Me.FlowLayoutPanel2)
        Me.pnlSource1.Controls.Add(Me.Label10)
        Me.pnlSource1.Controls.Add(Me.ddlSource1Type)
        Me.pnlSource1.Location = New System.Drawing.Point(5, 25)
        Me.pnlSource1.Name = "pnlSource1"
        Me.pnlSource1.Padding = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.pnlSource1.Size = New System.Drawing.Size(866, 342)
        Me.pnlSource1.TabIndex = 45
        Me.pnlSource1.TabStop = False
        Me.pnlSource1.Text = "Source 1"
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoSize = True
        Me.FlowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel2.Controls.Add(Me.pnlCsv1)
        Me.FlowLayoutPanel2.Controls.Add(Me.pnlQuery1)
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(9, 40)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(851, 289)
        Me.FlowLayoutPanel2.TabIndex = 48
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(11, 22)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(34, 13)
        Me.Label10.TabIndex = 47
        Me.Label10.Text = "Type:"
        '
        'pnlSource2
        '
        Me.pnlSource2.AutoSize = True
        Me.pnlSource2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.pnlSource2.Controls.Add(Me.Label11)
        Me.pnlSource2.Controls.Add(Me.ddlSource2Type)
        Me.pnlSource2.Controls.Add(Me.FlowLayoutPanel3)
        Me.pnlSource2.Location = New System.Drawing.Point(5, 368)
        Me.pnlSource2.Name = "pnlSource2"
        Me.pnlSource2.Padding = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.pnlSource2.Size = New System.Drawing.Size(866, 342)
        Me.pnlSource2.TabIndex = 46
        Me.pnlSource2.TabStop = False
        Me.pnlSource2.Text = "Source 2"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(11, 22)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(34, 13)
        Me.Label11.TabIndex = 49
        Me.Label11.Text = "Type:"
        '
        'ddlSource2Type
        '
        Me.ddlSource2Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlSource2Type.FormattingEnabled = True
        Me.ddlSource2Type.Location = New System.Drawing.Point(62, 19)
        Me.ddlSource2Type.Name = "ddlSource2Type"
        Me.ddlSource2Type.Size = New System.Drawing.Size(186, 21)
        Me.ddlSource2Type.TabIndex = 48
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.AutoSize = True
        Me.FlowLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel3.Controls.Add(Me.pnlCsv2)
        Me.FlowLayoutPanel3.Controls.Add(Me.pnlQuery2)
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(9, 40)
        Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(851, 289)
        Me.FlowLayoutPanel3.TabIndex = 50
        '
        'ckTrimStrings
        '
        Me.ckTrimStrings.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ckTrimStrings.AutoSize = True
        Me.ckTrimStrings.Location = New System.Drawing.Point(562, 758)
        Me.ckTrimStrings.Name = "ckTrimStrings"
        Me.ckTrimStrings.Size = New System.Drawing.Size(81, 17)
        Me.ckTrimStrings.TabIndex = 43
        Me.ckTrimStrings.Text = "Trim Strings"
        Me.ckTrimStrings.UseVisualStyleBackColor = True
        '
        'txtOutputPath
        '
        Me.txtOutputPath.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtOutputPath.Location = New System.Drawing.Point(79, 778)
        Me.txtOutputPath.Name = "txtOutputPath"
        Me.txtOutputPath.Size = New System.Drawing.Size(450, 20)
        Me.txtOutputPath.TabIndex = 26
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 781)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 13)
        Me.Label7.TabIndex = 48
        Me.Label7.Text = "Output path:"
        '
        'ckExportDatasets
        '
        Me.ckExportDatasets.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ckExportDatasets.AutoSize = True
        Me.ckExportDatasets.Location = New System.Drawing.Point(562, 774)
        Me.ckExportDatasets.Name = "ckExportDatasets"
        Me.ckExportDatasets.Size = New System.Drawing.Size(99, 17)
        Me.ckExportDatasets.TabIndex = 44
        Me.ckExportDatasets.Text = "Export datasets"
        Me.ckExportDatasets.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(715, 731)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(41, 13)
        Me.Label12.TabIndex = 49
        Me.Label12.Text = "Sorted:"
        '
        'ddlSorted
        '
        Me.ddlSorted.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ddlSorted.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlSorted.FormattingEnabled = True
        Me.ddlSorted.Location = New System.Drawing.Point(762, 728)
        Me.ddlSorted.Name = "ddlSorted"
        Me.ddlSorted.Size = New System.Drawing.Size(100, 21)
        Me.ddlSorted.TabIndex = 49
        '
        'ToolsToolStripMenuItem
        '
        Me.ToolsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.JoinSourcesToolStripMenuItem})
        Me.ToolsToolStripMenuItem.Name = "ToolsToolStripMenuItem"
        Me.ToolsToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.ToolsToolStripMenuItem.Text = "Tools"
        '
        'JoinSourcesToolStripMenuItem
        '
        Me.JoinSourcesToolStripMenuItem.Name = "JoinSourcesToolStripMenuItem"
        Me.JoinSourcesToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.JoinSourcesToolStripMenuItem.Text = "Join Sources"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(874, 804)
        Me.Controls.Add(Me.ddlSorted)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.ckExportDatasets)
        Me.Controls.Add(Me.txtOutputPath)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.ckTrimStrings)
        Me.Controls.Add(Me.pnlSource2)
        Me.Controls.Add(Me.pnlSource1)
        Me.Controls.Add(Me.ckIgnoreCase)
        Me.Controls.Add(Me.ckCoerceTypes)
        Me.Controls.Add(Me.txtIgnoreFields)
        Me.Controls.Add(Me.txtKeyFields)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnCompare)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Form1"
        Me.Text = "QueryDiff"
        Me.pnlQuery1.ResumeLayout(False)
        Me.pnlQuery1.PerformLayout()
        Me.pnlQuery2.ResumeLayout(False)
        Me.pnlQuery2.PerformLayout()
        Me.pnlCsv1.ResumeLayout(False)
        Me.pnlCsv1.PerformLayout()
        Me.pnlCsv2.ResumeLayout(False)
        Me.pnlCsv2.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.pnlSource1.ResumeLayout(False)
        Me.pnlSource1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.pnlSource2.ResumeLayout(False)
        Me.pnlSource2.PerformLayout()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCompare As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDb1 As System.Windows.Forms.TextBox
    Friend WithEvents txtDb2 As System.Windows.Forms.TextBox
    Friend WithEvents txtSql1 As System.Windows.Forms.TextBox
    Friend WithEvents txtSql2 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtKeyFields As System.Windows.Forms.TextBox
    Friend WithEvents txtIgnoreFields As System.Windows.Forms.TextBox
    Friend WithEvents ddlProvider1 As System.Windows.Forms.ComboBox
    Friend WithEvents ddlProvider2 As System.Windows.Forms.ComboBox
    Friend WithEvents pnlQuery1 As System.Windows.Forms.Panel
    Friend WithEvents pnlQuery2 As System.Windows.Forms.Panel
    Friend WithEvents pnlCsv1 As System.Windows.Forms.Panel
    Friend WithEvents txtCsv1 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnCsv1 As System.Windows.Forms.Button
    Friend WithEvents btnCsv2 As System.Windows.Forms.Button
    Friend WithEvents txtCsv2 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents pnlCsv2 As System.Windows.Forms.Panel
    Friend WithEvents ckCoerceTypes As System.Windows.Forms.CheckBox
    Friend WithEvents ckIgnoreCase As System.Windows.Forms.CheckBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoadSettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveSettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddlSource1Type As System.Windows.Forms.ComboBox
    Friend WithEvents pnlSource1 As System.Windows.Forms.GroupBox
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents pnlSource2 As System.Windows.Forms.GroupBox
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ddlSource2Type As System.Windows.Forms.ComboBox
    Friend WithEvents ckTrimStrings As System.Windows.Forms.CheckBox
    Friend WithEvents txtOutputPath As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ckExportDatasets As System.Windows.Forms.CheckBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents ddlSorted As System.Windows.Forms.ComboBox
    Friend WithEvents ToolsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JoinSourcesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
